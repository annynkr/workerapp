import React, { useState } from 'react';
import { connect } from 'react-redux';
import {
  CustomInput,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Label,
} from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from '../../components/common/CustomSelectInput';
import IntlMessages from '../../helpers/IntlMessages';

import { addTodoItem } from '../../redux/actions';

const initialState = {
  user: '',
  email: '',
  password: '',
  confirmPassword: '',
  fechaNC: '',
  cargo: '',
  name: '',
  telefono: '',
  celular: '',
  privilegios: {},
  direccion: '',
  status: 'PENDING',
};

const AddNewTodoModal = ({
  modalOpen,
  toggleModal,
  labels,
  categories,
  addTodoItemAction,
}) => {
  const [state, setState] = useState(initialState);

  const addNetItem = () => {
    const newItem = {
      user: state.user,
      email: state.email,
      password: state.password,
      confirmPassword: state.confirmPassword,
      fechaNC: state.fechaNC,
      cargo: state.cargo,
      name: state.name,
      telefono: state.telefono,
      celular: state.celular,
      privilegios: state.privilegios.value,
      direccion: state.direccion,
      status: state.status,
    };
    addTodoItemAction(newItem);
    toggleModal();
    setState(initialState);
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="Nuevo usuario" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="Usuario" />
        </Label>
        <Input
          type="text"
          defaultValue={state.user}
          onChange={(event) =>
            setState({ ...state, user: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Correo electronico" />
        </Label>
        <Input
          type="email"
          defaultValue={state.email}
          onChange={(event) =>
            setState({ ...state, email: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Contrasena" />
        </Label>
        <Input
          type="password"
          defaultValue={state.password}
          onChange={(event) =>
            setState({ ...state, password: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Confirmar contrasena" />
        </Label>
        <Input
          type="password"
          defaultValue={state.confirmPassword}
          onChange={(event) =>
            setState({ ...state, confirmPassword: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Fecha de nacimiento" />
        </Label>
        <Input
          type="date"
          defaultValue={state.fechaNC}
          onChange={(event) =>
            setState({ ...state, fechaNC: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Cargo" />
        </Label>
        <Input
          type="text"
          defaultValue={state.cargo}
          onChange={(event) =>
            setState({ ...state, cargo: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Nombre" />
        </Label>
        <Input
          type="text"
          defaultValue={state.name}
          onChange={(event) =>
            setState({ ...state, name: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Telefono" />
        </Label>
        <Input
          type="tel"
          defaultValue={state.telefono}
          onChange={(event) =>
            setState({ ...state, telefono: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Celular" />
        </Label>
        <Input
          type="tel"
          defaultValue={state.celular}
          onChange={(event) =>
            setState({ ...state, celular: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="Privilegios" />
        </Label>
        <Select
          components={{ Input: CustomSelectInput }}
          className="react-select"
          classNamePrefix="react-select"
          name="form-field-name"
          options={labels.map((x, i) => {
            return {
              label: x.label,
              value: x.label,
              key: i,
              color: x.color,
            };
          })}
          value={state.privilegios}
          onChange={(val) => setState({ ...state, privilegios: val })}
        />


        <Label className="mt-4">
          <IntlMessages id="Direccion" />
        </Label>
        <Input
          type="textarea"
          defaultValue={state.direccion}
          onChange={(event) =>
            setState({ ...state, direccion: event.target.value })
          }
        />

        <Label className="mt-4">
          <IntlMessages id="todo.status" />
        </Label>
        <CustomInput
          type="radio"
          id="exCustomRadio"
          name="customRadio"
          label="COMPLETED"
          checked={state.status === 'COMPLETED'}
          onChange={(event) =>
            setState({
              ...state,
              status: event.target.value === 'on' ? 'COMPLETED' : 'PENDING',
            })
          }
        />
        <CustomInput
          type="radio"
          id="exCustomRadio2"
          name="customRadio2"
          label="PENDING"
          defaultChecked={state.status === 'PENDING'}
          onChange={(event) =>
            setState({
              ...state,
              status: event.target.value !== 'on' ? 'COMPLETED' : 'PENDING',
            })
          }
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="Cancelar" />
        </Button>
        <Button color="primary" onClick={() => addNetItem()}>
          <IntlMessages id="Procesar" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ todoApp }) => {
  const { labels, categories } = todoApp;
  return {
    labels,
    categories,
  };
};
export default connect(mapStateToProps, {
  addTodoItemAction: addTodoItem,
})(AddNewTodoModal);
