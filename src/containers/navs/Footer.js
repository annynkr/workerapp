import React from 'react';
import { Row } from 'reactstrap';
import { Colxx } from '../../components/common/CustomBootstrap';

const Footer = () => {
  return (
    <footer className="page-footer">
      <div className="footer-content">
        <div className="container-fluid">
          <Row>
            <Colxx xxs="12" sm="8">
              <p className="mb-0 text-muted">© T-work Chile {(new Date().getFullYear())}. Todos los derechos reservados</p>
            </Colxx>
            <Colxx className="col-sm-4 d-none d-sm-block">
              <p className="mb-0 text-muted breadcrumb pt-0 pr-0 float-right">Versión 1.2.0</p>
            </Colxx>
          </Row>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
