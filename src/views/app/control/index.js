import React, { useState, useEffect } from 'react';
import {
  Row,
  Button,
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';

import IntlMessages from '../../../helpers/IntlMessages';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';

import {
  getTodoList
} from '../../../redux/actions';
import AddNewTodoModal from '../../../containers/applications/AddNewTodoModal';


const TodoApp = ({
  match,
  getTodoListAction,
}) => {
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    document.body.classList.add('right-menu');
    getTodoListAction();

    return () => {
      document.body.classList.remove('right-menu');
    };
  }, [getTodoListAction]);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <div className="mb-4">
            <h1>Control de usuarios</h1>
              <div className="text-zero top-right-button-container">
                <Button
                  color="primary"
                  size="md"
                  className="top-right-button"
                  onClick={() => setModalOpen(true)}
                >
                  <IntlMessages id="Nuevo usuario" />
                </Button>{' '}
              </div>
            <Breadcrumb match={match} />
            <Separator className="mb-5" />
          </div>
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" className="mb-4">
          <p>
            Holas
          </p>
        </Colxx>
      </Row>
      <AddNewTodoModal
        toggleModal={() => setModalOpen(!modalOpen)}
        modalOpen={modalOpen}
      />
    </>
  );
};

const mapStateToProps = () => {
};

export default injectIntl(
  connect(mapStateToProps, {
    getTodoListAction: getTodoList
  })(TodoApp)
);

// export default ControlUsers;
